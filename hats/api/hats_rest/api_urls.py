from django.urls import path
from .views import (
    api_list_hats,
    api_show_hat
)


urlpatterns = [
    path("hats_rest/", api_list_hats, name="api_create_hats"),
    path(
        "locations/<int:location_vo_id>/hats_rest/",
        api_list_hats,
        name="api_list_hats_rest",
    ),
    path("hats_rest/<int:id>/", api_show_hat, name="api_show_hat"),
]
