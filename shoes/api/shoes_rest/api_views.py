from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "id"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_Url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    print("**************************************** this is bin_vo_id", bin_vo_id)
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        # Get the Conference object and put it in the content dict
        print(content)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, id):
    try:
        shoe = Shoe.objects.get(id=id)
    except Shoe.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid shoe id"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            shoe, encoder=ShoeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(
                    import_href=content["bin"]
                )
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"messag": "Invalid bin id"},
                status=404,
            )
        Shoe.objects.filter(id=id).update(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
