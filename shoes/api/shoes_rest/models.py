from django.db import models

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    # number = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200, null=True)
    model_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    # picture_Url = models.URLField
    picture_Url = models.URLField(default="")
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        # related name for Attendee model connected to ConferenceVO is 'attendees'!!!
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    # def create_badge(self):
    #     try:
    #         self.badge
    #     except ObjectDoesNotExist:
    #         Badge.objects.create(attendee=self)

    # def get_api_url(self):
    #     return reverse("api_show_attendee", kwargs={"pk": self.pk})
