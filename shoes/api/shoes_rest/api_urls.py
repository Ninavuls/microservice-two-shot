from django.urls import path

from .api_views import api_list_shoes, api_show_shoe

urlpatterns = [
    path("shoes_rest/", api_list_shoes, name="api_create_shoe"),
    path(
        "wardrobe/<int:bin_vo_id>/shoes_rest/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path("shoes_rest/<int:id>/", api_show_shoe, name="api_show_shoe"),
]
